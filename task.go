package main

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
)

// Any structure
type Any struct {
	a int
}

// OptionalParams describes optional parameters: f -  int, s -  time.Time, t -  pointer to any struct
type OptionalParams struct {
	f interface{}
	s interface{}
	t interface{}
}

// ReturnParameters describes list of returned parameters
type ReturnParamsSet struct {
	required int
	first    int
	second   time.Time
	third    interface{}
}

func main() {
	a := 1
	ReturnParams(12, OptionalParams{f: 23, s: time.Now().AddDate(1, 0, 0), t: &Any{1}})
	ReturnParams(12, OptionalParams{f: 0, s: time.Now().AddDate(1, 0, 0), t: &a})

	fmt.Println(ReturnStruct("Tests passed: 2, failed: 173, muted: 6, ignored: 3; process exited with code 1"))
	fmt.Println(ReturnStruct("Tests passed: 64"))
	fmt.Println(ReturnStruct("Success"))

}

// returnParams returns 4 parameters based on 1 mandatory and 3 optional input parameters
func ReturnParams(zero int, p OptionalParams) ReturnParamsSet {
	first := 10
	second := time.Now()
	var third interface{}

	// checks if first optional param is int
	if p.f != nil && reflect.TypeOf(p.f).Kind() == reflect.Int {
		first = (p.f).(int)
	}
	// checks if second optional is time.Time
	if p.s != nil && reflect.TypeOf(p.s) == reflect.TypeOf(time.Time{}) {
		second = (p.s).(time.Time)
	}
	// checks if third optional is pointer to structure
	if p.t != nil && reflect.TypeOf(p.t).Kind() == reflect.Ptr && reflect.Indirect(reflect.ValueOf(p.t)).Kind() == reflect.Struct {
		third = p.t
	}

	fmt.Printf("Mandatory parameter: %v Optional parameters: int - %v , time.Time - %v, pointer - %v \n", zero, first, second, third)
	return ReturnParamsSet{zero, first, second, third}

}

// tests - structure for second task
type tests struct {
	failed  int64
	passed  int64
	ignored int64
}

// ReturnStruct returns structure filled by numbers from input string
func ReturnStruct(s string) tests {
	var f, p, in, m, ignored int
	var ef, ep, ein, em error

	l := strings.ToLower(s)
	if strings.Contains(l, "tests") {
		//replace "tests" and all gaps, then split by ","
		a := strings.Split(strings.Replace(strings.Replace(l, "tests", "", 1), " ", "", -1), ",")
		for i := 0; i < len(a); i++ {
			b := strings.Split(a[i], ":")
			switch b[0] {
			case "failed":
				f, ef = getInt(b[1])
			case "passed":
				p, ep = getInt(b[1])
			case "ignored":
				in, ein = getInt(b[1])
			case "muted":
				m, em = getInt(b[1])
			}

		}
		if ein == nil {
			if em == nil {
				ignored = in + m
			} else {
				ignored = in
			}
		} else {
			if em == nil {
				ignored = m
			}
		}

		return tests{failed: getInt64(f, ef), passed: getInt64(p, ep), ignored: int64(ignored)}
	} else {
		return tests{}
	}

}

// getInt gets only int from string
func getInt(s string) (int, error) {
	r := ""
	if s != "" {
		if len(s) == 1 {
			r = s
		} else {
			for j := 0; j < len(s); j++ {
				n, err := strconv.Atoi(string(s[j]))
				if err == nil {
					r += strconv.Itoa(n)
				} else {
					break
				}
			}
		}

	}
	return strconv.Atoi(r)
}

// getInt64 converts int to int64
func getInt64(i int, e error) int64 {
	var r int64
	if e == nil {
		r = int64(i)
	}
	return r
}
