package main

import (
	"fmt"
	"testing"
	"time"
)

const (
	TimeFormat = "2006-01-02"
)

func TestReturnParams(t *testing.T) {
	fmt.Println("****Set of test for Task 1****")

	fmt.Println("1. Case when all parameters passed")
	result1 := ReturnParams(25, OptionalParams{f: 15, s: time.Now().AddDate(1, 1, 0)})
	if result1.third != nil {
		t.Errorf("Third parameter is incorrect, got: %v, want: %v.", result1.third, nil)
	}

	fmt.Println("2. Case when only mandatory passed")
	results2 := ReturnParams(25, OptionalParams{})
	if results2.first != 10 {
		t.Errorf("First parameter is incorrect, got: %v, but default value: %v.", results2.first, 10)
	}
	expectedTime := time.Now().Format(TimeFormat)
	returnedTime := results2.second.Format(TimeFormat)
	if expectedTime != returnedTime {
		t.Errorf("Second parameter is incorrect, got: %v, but default valu: %v.", expectedTime, returnedTime)
	}
	if results2.third != nil {
		t.Errorf("Third parameter is incorrect, got: %v, but default value: %v.", results2.third, nil)
	}

	fmt.Println("3. Case when mandatory and first invalid optional passed")
	results3 := ReturnParams(12, OptionalParams{f: "0"})
	if results3.first != 10 {
		t.Errorf("Second parameter is incorrect, got: %v, but default value: %v.", results3.first, 10)
	}

	fmt.Println("4. Case when mandatory and first valid optional passed")
	results4 := ReturnParams(12, OptionalParams{f: 0})
	if results4.first != 0 {
		t.Errorf("Second parameter is incorrect, got: %v, but expected: %v.", results4.first, 0)
	}

	fmt.Println("5. Case when mandatory and third valid optional passed")
	results5 := ReturnParams(12, OptionalParams{t: &Any{4}})
	pointer := results5.third
	if pointer.(*Any).a != 4 {
		t.Errorf("Third parameter is incorrect, got: %v, but expected: %v.", pointer, 4)
	}

	fmt.Println("6. Case when mandatory and third invalid optional passed")
	a := 1
	time := time.Time{}
	results6 := ReturnParams(12, OptionalParams{s: time, t: &a})
	if results6.third != nil {
		t.Errorf("Third parameter is incorrect, got: %v, but expected: %v.", results6.third, nil)
	}
	if !results6.second.IsZero() {
		t.Errorf("Second parameter is incorrect, got: %v, but expected: %v.", results6.second, time)
	}

	fmt.Println("****Set of test for Task 2****")

	fmt.Println("1. Valid string with all parameters")
	result21 := ReturnStruct("Tests failed: 2(1 new), passed: 173, muted: 3,ignored: 6 ; process exited with code 1")

	if result21.failed != 2 {
		t.Errorf("Failed tests number is incorrect, got: %v, but expected: %v.", result21.failed, 2)
	}
	if result21.ignored != 9 {
		t.Errorf("Ignored tests number is incorrect, got: %v, but expected: %v.", result21.ignored, 9)
	}
	if result21.passed != 173 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result21.passed, 173)
	}

	fmt.Println("2. Valid string with passed ")
	result22 := ReturnStruct("Tests passed: 64")
	if result22.passed != 64 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result22.passed, 64)
	}

	fmt.Println("3. Valid string with passed and failed ")
	result23 := ReturnStruct("Tests failed: 5 (1 new), passed: 125; process exited with code 1")
	if result23.passed != 125 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result23.passed, 125)
	}
	if result23.failed != 5 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result23.failed, 5)
	}

	fmt.Println("4. Valid string with all parameters ")
	result24 := ReturnStruct("Tests passed: 2, failed: 173, muted: 6, ignored: 3; process exited with code 1")
	if result24.passed != 2 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result24.passed, 2)
	}
	if result24.failed != 173 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result24.failed, 173)
	}
	if result24.ignored != 9 {
		t.Errorf("Passed tests number is incorrect, got: %v, but expected: %v.", result24.ignored, 9)
	}

	fmt.Println("5. Invalid string")
	result25 := ReturnStruct("Success")
	if result25.ignored != 0 || result25.failed != 0 || result25.passed != 0 {
		t.Errorf("Response is incorrect, got: ignored %v,failed %v, passed %v, but expected: empty structure.", result25.ignored, result25.failed, result25.passed)
	}

	fmt.Println("6. Invalid string containing Tests")
	result26 := ReturnStruct("Tests failed:")
	if result26.ignored != 0 || result26.failed != 0 || result26.passed != 0 {
		t.Errorf("Response is incorrect, got: ignored %v,failed %v, passed %v, but expected: empty structure.", result25.ignored, result25.failed, result25.passed)
	}

}
