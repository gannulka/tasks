# tasks
1) function ReturnParams

Wrote a function that takes a one required parameter int and three optional:

first - int, default value 10

second - time.Time, default time.Now()

third - pointer to any struct, default - nil

The function should return all passed parameters.

Wrote unit tests for this function.

2) function ReturnStruct

Wrote a function that takes a string and parses it to struct
type tests struct {
	failed  int64
	passed  int64
	ignored int64
}
The function should return the tests struct.

examples of valid input:

Tests passed: 64

Tests failed: 2 (1 new), passed: 173, ignored: 6, muted: 3; process exited with code 1

Tests failed: 4 (3 new), passed: 55; process exited with code 1

Tests failed: 5 (1 new), passed: 125; process exited with code 1

Tests failed: 1 (1 new), passed: 310, ignored: 13; process exited with code 1

Tests passed: 311, ignored: 13

Tests failed: 1 (1 new), passed: 0; process exited with code 1

Tests failed: 3 (2 new), passed: 174, ignored: 7, muted: 1; process exited with code 1

Tests passed: 2, failed: 173, muted: 6, ignored: 3; process exited with code 1

Tests failed: 15, passed: 151; process exited with code 1

! muted tests should be treated as ignored

if cannot parse string, return empty struct, examples:

Success

Process exited with code 1

Canceled

Snapshot dependency builds failed: 1

Canceled (Process exited with code 137)

Wrote unit tests for this function.
